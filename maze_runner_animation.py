from __future__ import print_function
from maze_generator import maze
from maze_runner import Agent
from matplotlib.animation import FuncAnimation, writers
import matplotlib.pyplot as plt
import matplotlib
import random

INIT_POS = (1, 4)
INIT_POS = None

# TEST GRID - 151208
# 566 moves by current DFS (OK)
# 532 moves by DFS + correct shortest-path
#   (current version is incorrect:
#    it should always end at home, same as in the case of pure DFS)
grid = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

grid = (maze(40, 40)*1).tolist()
fig, axarr = plt.subplots(1, 2)

ax = axarr[0]
ax2 = axarr[1]

ttl = ax.text(.5, 1.05, "", transform=ax.transAxes, va='center')
ttl2 = ax2.text(.5, 1.05, "", transform=ax2.transAxes, va='center')


# create agent for animation purposes
ANIMATED = True
agent = Agent(grid, init_pos=INIT_POS, animation=ANIMATED, optimal=False,
              mixed=True)
agent2 = Agent(grid, init_pos=agent.init_pos, animation=ANIMATED,
               optimal=True)

im = ax.imshow(agent.color_map(), interpolation='none', animated=True,
               vmin=0, vmax=1)
im2 = ax2.imshow(agent2.color_map(), interpolation='none', animated=True,
                 vmin=0, vmax=1)


def data_gen():
    """First explores maze and draws map then
    searches for shortest path."""

    while not (agent.map_is_known and agent2.map_is_known):
        map1, steps1 = agent.explore(), agent.steps
        map2, steps2 = agent2.explore(), agent2.steps
        print(steps1, end="\r")
        yield {
            1: [map1, steps1],
            2: [map2, steps2]
        }

    # if agent.current_pos != agent.init_pos:
    #     target = agent.init_pos
    # else:
    #     target = (len(grid) - 2, len(grid[0]) - 2)

    # while
    gen1 = agent.find_shortest_path(agent.current_pos)
    gen2 = agent2.find_shortest_path(agent2.current_pos)

    while not (agent.found_path_back and agent2.found_path_back):
        try:
            next(gen1)
        except:
            pass

        try:
            next(gen2)
        except:
            pass

        map1, steps1 = agent.color_map(), agent.steps
        map2, steps2 = agent2.color_map(), agent2.steps

        yield {
            1: [map1, steps1],
            2: [map2, steps2]
        }

    back_steps1 = len(agent.calculated_path)
    back_steps2 = len(agent2.calculated_path)
    print("Exploration finished")
    print("EXPLORATION STEPS:", steps1, steps2)
    print("GOING BACK STEPS:", back_steps1, back_steps2)
    print("TOTAL:", back_steps1+steps1, back_steps2+steps2)


def animate(data):
    print("Agent1 ({}), Agent2 ({}) ({})".format(
        data[1][1], data[2][1], data[1][1] == data[2][1]), end="\r")
    im.set_data(data[1][0])
    im2.set_data(data[2][0])
    ttl.set_text("DFS:{}".format(data[1][1]))
    ttl2.set_text("MIXED:{}".format(data[2][1]))
    return [im, im2, ttl, ttl2]

anim = FuncAnimation(fig, animate, data_gen,
                     interval=1, blit=False, repeat=False,
                     save_count=2600)


plt.show()

# Writer = writers['ffmpeg']
# writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
# anim.save("maze.mp4", writer=writer)
# plt.show()
# anim.save('astar.mp4')
# anim.save('mixed-example.gif', writer='imagemagick', fps=5)
