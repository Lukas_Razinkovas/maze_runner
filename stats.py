from __future__ import print_function
from maze_generator import maze
from maze_runner import Agent
import numpy as np
import matplotlib.pyplot as plt


def explore_map(size=20, mode=0):
    grid = (maze(size, size, complexity=0.8, density=1)*1).tolist()
    if mode == 0:
        agent = Agent(grid, init_pos=[3, 3], mixed=False, optimal=False)
    elif mode == 1:
        agent = Agent(grid, init_pos=[3, 3], mixed=True, optimal=False)
    else:
        agent = Agent(grid, init_pos=[3, 3], mixed=False, optimal=True)

    while not agent.map_is_known:
        agent.explore()

    return agent.steps


x = []
y_dfs = []
y_mixed = []
y_optimal = []


for t, mixed in [[y_dfs, 0], [y_mixed, 1], [y_optimal, 2]]:
    print("#"*70)
    print(mixed)
    print("#"*70)
    x = []
    for size in np.arange(10, 35, 5):
        x.append(size)
        print("-"*70)
        print(size)
        print("-"*70)
        total = []
        for i in range(30):
            total.append(explore_map(size, mixed))
            print(i, total[-1], end="\r")
        t.append(np.mean(total))


plt.plot(x, y_dfs, "bo", label="dfs")
plt.plot(x, y_dfs, "b-")
plt.plot(x, y_mixed, "ro", label="mixed")
plt.plot(x, y_mixed, "r-")
plt.plot(x, y_optimal, "go", label="optimal")
plt.plot(x, y_optimal, "g-")
plt.xlabel("maze size $X$ ($X\\times X$)")
plt.ylabel("avg. steps")
plt.grid()
plt.legend()
plt.savefig("comparison.jpeg", dpi=150)
plt.show()
