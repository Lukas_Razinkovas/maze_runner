from __future__ import print_function
from maze_generator import maze
from maze_runner import Agent
import numpy as np
import matplotlib.pyplot as plt


grid = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
    [1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]


def explore_map(size=20, mode=0):
    if mode == 0:
        agent = Agent(grid, mixed=False, optimal=False)
    elif mode == 1:
        agent = Agent(grid, mixed=True, optimal=False)
    else:
        agent = Agent(grid, mixed=False, optimal=True)

    while not agent.map_is_known:
        agent.explore()

    steps = agent.steps
    for i in agent.find_shortest_path(agent.current_pos):
        steps += 1

    return steps


x = []
y_dfs = []
y_mixed = []
y_optimal = []

for t, mixed in [[y_dfs, 0], [y_mixed, 1], [y_optimal, 2]]:
    for i in range(100):
        t.append(explore_map(mode=mixed))
        print(i, t[-1], end="\r")

print("DFS", np.array(y_dfs).mean())
print("MIXED", np.array(y_mixed).mean())
print("OPTIMAL", np.array(y_optimal).mean())

plt.hist(y_dfs, 7, label="dfs", alpha=0.5, normed=True)
plt.hist(y_optimal, 7, label="optimal", alpha=0.5, normed=True)
plt.hist(y_mixed, 30, label="mixed", alpha=0.5, normed=True)
plt.title("Test Maze 'Explore and Return' Analysis")
plt.xlabel("Number of steps")
plt.ylabel("P")


# plt.grid()RR
plt.legend()
plt.savefig("final_stats.jpeg", dpi=150)
plt.show()
