"""
===================================
NAMU DARBAS: KLAJONES PO LABIRINTA
===================================
Lukas Razinkovas
"""

import copy
import pprint
import logging
import numpy as np
import time
import random

log = logging.getLogger()
log.setLevel(logging.DEBUG)
pp = pprint.PrettyPrinter()


class Maze(object):
    FREE = 0
    WALL = 1


class MapMarks(object):
    UNKNOWN = "'"
    VISITED = " "
    WALL = "#"
    CURRENT = "O"
    PATH = "@"

    # A* markings
    CLOSED = "c"
    OPEN = "o"
    TARGET = "T"

    @classmethod
    def get_color_repr(cls, mp):
        mp = np.array(mp)
        rep = (
            (mp == cls.UNKNOWN)*0 +
            (mp == cls.VISITED)*0.7 +
            (mp == cls.WALL)*1 +
            (mp == cls.CURRENT)*0.8 +
            (mp == cls.PATH)*0.3 +
            (mp == cls.CLOSED)*0.5 +
            (mp == cls.OPEN)*0.4 +
            (mp == cls.TARGET)*0.2
        )
        return rep


def add_margins(iterable, fill=MapMarks.UNKNOWN):
    """Extends given maze with margins,
    also sets every element as UNKNOWN.
    Works for any dimensional case.
    """
    if isinstance(iterable, list):
        new_iterable = []
        for i in iterable:
            new_iterable.append(add_margins(i))
        new_iterable.append(copy.deepcopy(new_iterable[-1]))
        new_iterable.append(copy.deepcopy(new_iterable[-1]))
        return new_iterable

    return fill


def select_point(array, points):
    """Given two lists return element specified by
    the second list.

    Example:
        >> select_point([[1, 2], [3, 4]], [1, 1])
        >> 4
    """
    if isinstance(array, list):
        return select_point(array[points[0]], points[1:])
    else:
        return array


class Agent(object):
    def __init__(self, maze, init_pos=None, ax=None, timeout=None,
                 animation=False, mixed=False, optimal=False,
                 look_arround=False):
        """Maze explorer. Explores maze and produces a map.
        If map is known can find shortest paths between points.
        Produced Map is bigger than maze matrix, because margins
        are included.

        Args:
          mixed (bool[True]): if True mooves forward as deep as possible
               mooves backward with BFS alghorithm
        """
        self.maze = maze
        self.create_empty_map()
        self.init_pos = (self.random_position() if init_pos is None
                         else tuple(init_pos))
        self.steps = 0
        self.mixed = mixed
        self.optimal = optimal

        if optimal:
            self.mixed = False

        self.look_arround = look_arround and mixed
        self.lastack = set()

        # For DFS alghorithm
        self.path_stack = [self.init_pos]
        self.map_is_known = False
        # Agent doesn't know maze size
        # so he lists all seen but unexplored paths
        self.unexplored = set()
        self.calculated_path = []

        # For *A alghorithm
        self.path_is_found = False
        self.animation = animation

        self.found_path_back = False
        self.mark_on_map(self.init_pos, MapMarks.CURRENT)
        self.unexplored_crossroads = []

    def random_position(self):
        while True:
            y = random.randint(0, len(self.maze) - 1)
            x = random.randint(0, len(self.maze[0]) - 1)
            if self.maze[y][x] == 0:
                return (x, y)

    def create_empty_map(self):
        """Creates an empty map for agent.
        Aditional fields are created for margin markings.
        Can be used for any dimensional case."""
        self.map = add_margins(self.maze)

    ##############################
    #      MAP EXPLORATION
    ##############################

    # -----------------------------
    #      MAZE  <-->  MAP
    # -----------------------------

    def _get_map_point(self, point):
        """Returns reference (list, index) to map
        for mutable changes."""
        lst = self.map
        for i in point[:-1]:
            lst = lst[i+1]

        return lst, point[-1] + 1

    def mark_on_map(self, point, state):
        """Marks point on the map."""
        # log.debug("Marking:", point, state)
        lst, idx = self._get_map_point(point)
        lst[idx] = state

        if point in self.unexplored:
            self.unexplored.remove(point)

    def map_value(self, point):
        """Returns value from agents map."""
        return select_point(self.map, [i+1 for i in point])

    def is_explored(self, point):
        return self.map_value(point) != MapMarks.UNKNOWN

    def is_open(self, point):
        return self.map_value(point) not in {MapMarks.UNKNOWN, MapMarks.WALL}

    # -----------------------------
    #         NAVIGATION
    # -----------------------------

    @property
    def current_pos(self):
        return self.path_stack[-1]

    def moove_to(self, point):
        """Sets new position as current and marks previous
        as visited."""
        # print("MOOVING FROM {} TO {}".format(self.current_pos, point))
        self.mark_on_map(self.current_pos, MapMarks.VISITED)
        if len(self.path_stack) >= 2 and point == self.path_stack[-2]:
            # print(" - POPED")
            self.path_stack.pop()
        else:
            self.path_stack.append(point)
        self.mark_on_map(self.current_pos, MapMarks.CURRENT)
        self.steps += 1

    def get_paths(self, point=None):
        """Returns all possible paths from given possition."""
        if point is None:
            point = self.current_pos

        paths = []

        # For many dimensional case we can moove forward
        # and backwart in any dimension
        for nr, i in enumerate(point):
            for step in [1, -1]:
                new_point = list(point)
                new_point[nr] += step
                paths.append(tuple(new_point))

        return paths

    def is_accessible(self, point):
        """Checks if wall is not blocking.
        """
        # if point is in margins
        if [i for i in point if i < 0]:
            return False

        try:
            val = select_point(self.maze, point)
            if val != Maze.WALL:
                return True
        except IndexError:
            return False

        return False

    def path_back(self):
        # If DFS is used
        if self.optimal:
            path = self.find_path_to_last_crossroad()
            return path
        elif self.mixed:
            path = self.find_path_to_nearest_unexplored()
            return path
        else:
            return [self.path_stack[-2]]

    def explore(self):
        """Uses DFS alghorithm. Recursion ommited because of
        limited recursion deptht in python."""
        if self.map_is_known:
            return self.color_map() if self.animation else 1

        curr_pos = self.current_pos
        if self.calculated_path:
            self.moove_to(self.calculated_path.pop(0))
            return self.color_map() if self.animation else 1

        frontier = self.get_paths()

        # start scanning directions
        moved = False
        while frontier:
            i = frontier.pop(0)

            if self.is_explored(i):
                continue

            # Moves to next unvisited field
            if self.is_accessible(i):
                # if self.look_arround and [
                #         k for k in frontier if not self.is_explored(k)]:
                #     self.mark_on_map(i, MapMarks.VISITED)
                #     # self.lastack.add(i)
                #     self.unexplored.update(
                #         {k for k in self.get_paths(i)
                #          if not self.is_explored(k)})
                #     # print("CUR_POS:", self.current_pos)
                #     # print("MARKED:", i)
                #     # input(self.unexplored)
                #     self.steps += 1
                #     return self.color_map() if self.animation else 1

                self.moove_to(i)
                moved = True
                break

            self.mark_on_map(i, MapMarks.WALL)
            self.steps += 1
            return self.color_map() if self.animation else 1

        if moved:
            unexplored = {i for i in frontier if not self.is_explored(i)}
            self.unexplored.update(unexplored)
            if unexplored and curr_pos not in self.unexplored_crossroads:
                self.unexplored_crossroads.append(curr_pos)
        else:
            # if self.look_arround and self.lastack:
            #     self.moove_to(self.lastack.pop())
            #     self.lastack = set()
            if self.unexplored:
                self.calculated_path = self.path_back()
                self.moove_to(self.calculated_path.pop(0))
            else:
                self.map_is_known = True
                return MapMarks.get_color_repr(self.map)

        return self.color_map() if self.animation else 1

    ##############################
    #       BFS alghorithm
    ##############################

    def construct_path(self, node, parents):
        """Returns complete path from node to target given parents"""
        # print("CONSTRUCTING:", node)
        # pp.pprint(parents)
        path = []
        while node in parents:
            self.mark_on_map(node, MapMarks.PATH)
            path.append(node)
            node = parents[node]

        return list(reversed(path))

    def bfs(self, fr, dest, include_dest=False):
        stack = [fr]
        parents = {}

        while stack:
            node = stack.pop(0)
            for nxt in self.get_paths(node):
                if nxt in parents.values():
                    continue

                # If found unexplored
                if nxt in dest:
                    if include_dest:
                        parents[nxt] = node
                        return self.construct_path(nxt, parents)
                    return self.construct_path(node, parents)

                if self.is_open(nxt) and nxt not in stack:
                    parents[nxt] = node
                    stack.append(nxt)

        raise Exception("Not Found")

    def find_path_to_nearest_unexplored(self):
        # print("Searching from:", self.current_pos)
        return self.bfs(self.current_pos, self.unexplored)

    def find_path_to_last_crossroad(self):
        # print("Searching from:", self.current_pos)
        dest = self.unexplored_crossroads.pop()
        # print("SEARCHING:", self.current_pos, "-->", dest)
        return self.bfs(self.current_pos, [dest], include_dest=True)

    ##############################
    #      A* alghorithm
    ##############################

    def get_possible_paths(self, point):
        """Positions where agent can moove from given point"""

        return {i for i in self.get_paths(point)
                if self.map_value(i) != Maze.WALL}

    def distance(self, point, point2=None):
        if point2 is None:
            point2 = self.init_pos

        return np.sqrt(pow(np.array(point) - np.array(point2), 2).sum())

    def find_shortest_path(self, start_pos=None, end_pos=None):
        """Uses A* alghorithm.
        heuristic function = straight distance between points.
        """
        start_pos = self.current_pos if start_pos is None else start_pos
        target = self.init_pos if end_pos is None else end_pos

        assert self.map_value(start_pos) != MapMarks.WALL, "{}|{}".format(
            start_pos, self.map_value(start_pos))
        assert self.map_value(target) != MapMarks.WALL, "{}|{}".format(
            target, self.map_value(start_pos))

        self.mark_on_map(target, MapMarks.TARGET)

        open_set = {start_pos}
        closed_set = set()
        g_scores = {start_pos: 0}
        parents = {}

        while open_set:
            # Get node with lowes cost function
            current = min(
                open_set, key=lambda x: self.distance(x, target) + g_scores[x])

            # If path is found break cycle
            if target in open_set:
                self.found_path_back = True
                self.calculated_path = self.construct_path(target, parents)
                for pth in self.calculated_path:
                    self.mark_on_map(pth, MapMarks.PATH)

                self.mark_on_map(target, MapMarks.TARGET)
                self.mark_on_map(start_pos, MapMarks.CURRENT)
                yield self.color_map() if self.animation else 1
                return

            open_set.remove(current)
            closed_set.add(current)
            self.mark_on_map(current, MapMarks.CLOSED)

            # Iterate through children and update open_set
            for p in self.get_paths(current):
                if p in closed_set or self.map_value(p) == MapMarks.WALL:
                    continue

                # Change connection if cost score is lower
                if g_scores.get(p, np.inf) > g_scores[current] + 1:
                    g_scores[p] = g_scores[current] + 1
                    parents[p] = current

                open_set.add(p)
                self.mark_on_map(p, MapMarks.OPEN)

            yield self.color_map() if self.animation else 1

    def color_map(self):
        """Representation for matplotlib image display."""
        return MapMarks.get_color_repr(self.map)


if __name__ == "__main__":
    grid = [[0, 0, 0, 0, 0, 1],
            [1, 1, 0, 0, 0, 1],
            [0, 0, 0, 1, 0, 0],
            [0, 1, 1, 0, 0, 1],
            [0, 1, 0, 0, 1, 1],
            [0, 1, 0, 0, 0, 1]]

    grid = [[0, 0, 0, 0, 0, 1],
            [1, 1, 0, 0, 0, 1],
            [0, 0, 0, 1, 0, 0],
            [0, 1, 1, 0, 1, 1],
            [0, 1, 1, 0, 1, 1],
            [0, 1, 1, 0, 1, 1]]

    agent = Agent(grid, init_pos=[3, 3], optimal=True)
    pp.pprint(agent.map)

    while not agent.map_is_known:
        agent.explore()
        pp.pprint(agent.map)
        print(agent.current_pos)
        time.sleep(0.3)

    print(agent.steps)
